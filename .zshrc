# my .zshrc file
#
# source  ~/.zsh


#my default editor is neovim
	EDITOR=nvim

#runs on startup
	neofetch --disable uptime de wm wm theme

# general aliases
	alias update='brew update -v  && brew upgrade -v'
	alias nvimconfig='nvim ~/.config/nvim/init.vim'
	alias vim='nvim'
	alias check='shasum -a 256'

#ls aliases
	alias ls='ls -GH'
	alias ll='ls -allGH'

#cd alises
	alias .='cd ..'
	alias ..='cd ../..'
	alias ...='cd ../../..'

#functions
	#makes a new directory in the current one and switches to it
	function mkcd () { 
		NAME=$1	
		mkdir $NAME
		cd $NAME
	}

eval "$(starship init zsh)"	
