"-----------------------------------------------------------------------------------------
"My VIM/NeoVIM config file
"Nothing too fancy, still learning how to make VIM my own
"-Kyle Kinney
"-----------NOTES-------------------------------------------------------------------------
"to enable vim-plug go to https://github.com/junegunn/vim-plug
"plugins pull from github unless specified
"-----------------------------------------------------------------------------------------

"sets up and runs plugins
 call plug#begin('~/.local/share/nvim/plugged')   "uses vim-plug to handel plugins
	Plug 'itchyny/lightline.vim'		"install lightline banner plugin
        Plug 'dracula/vim', {'as':'dracula'}	"sets dracula theme 
        Plug 'frazrepo/vim-rainbow'           	"helps with visulizing brakets
        Plug 'tpope/vim-surround'             	"makes it easy to surround words with brakets/quotes or change them
 call plug#end()

 "dracula color scheme 
 	colorscheme dracula		 	"enables colorscheme
	
"lightline settings
	let g:lightline = {
      	\ 'colorscheme': 'wombat',
      	\ }					"sets colorscheme
"-----------------------------------------------------------------------------------------
 "OTHER SETTINGS
"-----------------------------------------------------------------------------------------
 	let g:rainbow_active = 1        "enables vim-rainbow plugin
